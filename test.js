const escpos = require('./');
const printer = new escpos({
		port: '/dev/lp/by-id/usb-68680200_TECH_CLA58_YAEN202107140001',
		//~ port: '/dev/lp/by-id/usb-067b2305_Prolific_Technology_Inc._IEEE-1284_Controller',
	});

(async () => {
	await printer.connect();
	await printer.begin()
			.barcode('CODE39', '987654321')
			.font('B')
			.text(`\tTIME\n`)
			.text(`DATE     : DD/MM/YYYY"\n`)
			.text(`START    : HH:mm:ss\n`)
			.text(`FINISH   : HH:mm:ss\n`)
			.text(`DURATION : 00:11:22\n\n`)
			.text(`\tMETER\n`)
			.text(`START    : 1 Liters\n`)
			.text(`FINISH   : 4 Liters\n`)
			.text(`TOTALIZER: 3 Liters\n\n`)
			.cut('B')
			.release()
			.print();
})();
