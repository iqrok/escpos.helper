const binNum = require('@iqrok/binnum');
const fs = require('fs');
const __serial = require('@iqrok/serial.helper');

class __escpos{
	constructor(config, debug){
		const self = this;
		self.config = config;
		self.debug = debug;
		self.CONSTANTS = require('./constant.js');
		self._message = [];
		self._mode = 0;
		self._serial = undefined;
	}

	/**
	 *	Connect to serial port and add event listeners
	 * */
	async connect(){
		const self = this;

		if(self.config.baud == undefined) return false;

		self._serial = new __serial({
				...self.config,
				autoreconnect: false,
				autoopen: false,
				parser: {
					type: 'InterByteTimeout',
					interval: 30,
				}
			}, true);

		self._serial.on('open', received => {
			console.log('open', received);
		});

		self._serial.on('error', received => {
			console.error('error', received);
		});

		return self._serial
			.connect()
			.catch(error => {
				console.error(error);
			});
	}

	_appendBytes(arrBytes){
		const self = this;
		self._message.push(Buffer.from(arrBytes));
	}

	begin(){
		const self = this;
		self._message = [];
		self._mode = 0;

		self._appendBytes(self.CONSTANTS.HW_INIT);

		return this;
	}

	text(str){
		const self = this;
		self._appendBytes(str);

		return this;
	}

	center(str, width = 32){
		const self = this;

		const pad = Math.round(width / 2) - Math.round(str.length / 2);
		const text = ' '.padStart(pad, ' ') + str;

		self._appendBytes(text);

		return this;
	}

	pad(str, max, character = ' ') {
		const self = this;
		str = str.toString();
		return str.length < max ? self.pad(character + str, max) : str;
	}


	bold(option = false){
		const self = this;
		self._mode = binNum(self._mode).bit(self.CONSTANTS.MODE_BIT.BOLD.bit).set(+option);

		return this;
	}

	underline(option = false){
		const self = this;
		self._mode = binNum(self._mode).bit(self.CONSTANTS.MODE_BIT.UNDERLINE.bit).set(+option);

		return this;
	}

	font(option = false){
		const self = this;

		switch(option.toUpperCase()){
			case 'A':
				option = 0x00;
				break;

			case 'B':
				option = 0x01;
				break;
		}

		self._mode = binNum(self._mode).bit(self.CONSTANTS.MODE_BIT.FONT.bit).set(+option);

		return this;
	}

	dblHeight(option = 'LEFT'){
		const self = this;
		self._mode = binNum(self._mode).bit(self.CONSTANTS.MODE_BIT.DBL_HEIGHT.bit).set(+option);

		return this;
	}

	dblWidth(option = 'LEFT'){
		const self = this;
		self._mode = binNum(self._mode).bit(self.CONSTANTS.MODE_BIT.DBL_WIDTH.bit).set(+option);

		return this;
	}

	setMode(options = [1, 0, 0, 1, 1, 1, 1, 1]){
		const self = this;

		self._appendBytes([
				...self.CONSTANTS.CMD_SET_PRINT_MODE,
				self._mode
			]);

		return this;
	}

	/**
	 *	size [A,B,FULL,PARTIAL]
	 * */
	cut(option = 'A'){
		const self = this;
		option = option.toUpperCase();

		const _PAPER_CUT_SIZE = self.CONSTANTS.PAPER_CUT_SIZE[option] !== undefined
			? self.CONSTANTS.PAPER_CUT_SIZE[option]
			: self.CONSTANTS.PAPER_CUT_SIZE.A;

		self._appendBytes([
				...self.CONSTANTS.CMD_PAPER_CUT,
				_PAPER_CUT_SIZE
			]);

		return this;
	}

	barcode(type, str, options = {}){
		const self = this;

		if(!self.CONSTANTS.BARCODE_TYPE[type]) return;

		const { height, width } = options;

		if(typeof(height) === 'number'){
			self._appendBytes([
					...self.CONSTANTS.BARCODE_HEIGHT,
					height,
				]);
		}

		if(typeof(width) === 'number'){
			self._appendBytes([
					...self.CONSTANTS.BARCODE_WIDTH,
					width,
				]);
		}

		self._appendBytes(self.CONSTANTS.BARCODE_HEADER);
		self._appendBytes(self.CONSTANTS.BARCODE_TYPE[type]);
		self.text(str);
		self._appendBytes([ 0x00 ]);

		return this;
	}

	release(str){
		const self = this;
		self._appendBytes(self.CONSTANTS.RELEASE);

		return this;
	}

	_build(){
		const self = this;

		self.setMode();
		const packet = Buffer.concat([...self._message]);
		self._message = [];
		self._mode = 0;

		return packet;
	}

	async print(){
		const self = this;

		const packet = self._build();

		return self._serial ? self.printSerial(packet) : self.printLp(packet);
	}

	async printSerial(packet){
		const self = this

		if(!self._serial.isOpen){
			console.error('Printer is not open');
			return;
		}

		return self._serial.write(packet);
	}

	async printLp(packet){
		const self = this;

		return fs.promises.writeFile(self.config.port, packet, 'binary')
			.then(rec => true)
			.catch(error => {
				console.error('Print error', error);
				return false;
			});
	}

	setPort(port){
		const self = this;
		return self._serial ? self.setPortSerial(port) : self.setPortLp(port);
	}

	setPortSerial(port){
		const self = this;
		console.error('Serial change port is not implemented yet!');
		return false;
	}

	setPortLp(port){
		const self = this;

		self.config.port = port;
		return self.config.port;
	}
};

module.exports = __escpos
