module.exports = {
	// Printer hardware
	HW_INIT						: [ 0x1b, 0x40 ],				// Clear data in buffer and reset modes
	HW_SELECT					: [ 0x1b, 0x3d, 0x01 ],			// Printer select
	HW_RESET					: [ 0x1b, 0x3f, 0x0a, 0x00 ],	// Reset printer hardware

	// Feed control sequences
	CTL_LF						: [ 0x0a ],					// Print and line feed
	CTL_FF						: [ 0x0c ],					// Form feed
	CTL_CR						: [ 0x0d ],					// Carriage return
	CTL_HT						: [ 0x09 ],					// Horizontal tab
	CTL_VT						: [ 0x0b ],					// Vertical tab

	// Paper cut size
	CMD_PAPER_CUT					: [ 0x1d, 0x56 ],
	PAPER_CUT_SIZE				: {
		FULL	: 0x00, // Full Size
		PARTIAL	: 0x01, // Partial Cut
		A		: 0x41, // A Size
		B		: 0x42, // B Size
	},

	// Release
	RELEASE					: [ 0x1b, 0x71],				// Release

	// Cash Drawer
	CD_KICK_2					: [ 0x1b, 0x70, 0x00 ],				// Send pulse to pin 2
	CD_KICK_5					: [ 0x1b, 0x70, 0x01 ],				// Send pulse to pin 5

	// Code Pages
	CP_SET						: [ 0x1b, 0x74 ],						// Set Code Page
	CP_CP437					: [ 0x0 ],
	CP_CP850					: [ 0x2 ],
	CP_CP860					: [ 0x3 ],
	CP_CP863					: [ 0x4 ],
	CP_CP865					: [ 0x5 ],
	CP_CP1251					: [ 0x6 ],
	CP_CP866					: [ 0x7 ],
	CP_MACCYRILLIC				: [ 0x8 ],
	CP_CP775					: [ 0x9 ],
	CP_CP1253					: [ 0x10 ],
	CP_CP737					: [ 0x11 ],
	CP_CP857					: [ 0x12 ],
	CP_ISO8859_9				: [ 0x13 ],
	CP_CP864					: [ 0x14 ],
	CP_CP862					: [ 0x15 ],
	CP_ISO8859_2				: [ 0x16 ],
	CP_CP1253_ALT				: [ 0x17 ],
	CP_CP1250					: [ 0x18 ],
	CP_CP858					: [ 0x19 ],
	CP_CP1254					: [ 0x20 ],
	CP_CP737_ALT				: [ 0x24 ],
	CP_CP1257					: [ 0x25 ],
	CP_CP847					: [ 0x26 ],
	CP_CP885					: [ 0x28 ],
	CP_CP857_ALT				: [ 0x29 ],
	CP_CP1250_ALT				: [ 0x30 ],
	CP_CP775_ALT				: [ 0x31 ],
	CP_CP1254_ALT				: [ 0x32 ],
	CP_CP1256					: [ 0x34 ],
	CP_CP1258					: [ 0x35 ],
	CP_ISO8859_2_ALT			: [ 0x36 ],
	CP_ISO8859_3				: [ 0x37 ],
	CP_ISO8859_4				: [ 0x38 ],
	CP_ISO8859_5				: [ 0x39 ],
	CP_ISO8859_6				: [ 0x40 ],
	CP_ISO8859_7				: [ 0x41 ],
	CP_ISO8859_8				: [ 0x42 ],
	CP_ISO8859_9_ALT			: [ 0x43 ],
	CP_ISO8859_15				: [ 0x44 ],
	CP_CP856					: [ 0x47 ],
	CP_CP874					: [ 0x47 ],

	// Text formating
	TXT_NORMAL					: [ 0x1b, 0x21, 0x00 ],		// Normal text
	TXT_2HEIGHT					: [ 0x1b, 0x21, 0x10 ],		// Double height text
	TXT_2WIDTH					: [ 0x1b, 0x21, 0x20 ],		// Double width text
	TXT_4SQUARE					: [ 0x1b, 0x21, 0x30 ],		// Quad area text

	// print mmode
	CMD_SET_PRINT_MODE			: [ 0x1b, 0x21 ],
	MODE_BIT: {
		FONT: {
			bit: 0,
			A: 0x00,
			B: 0x01,
		},

		BOLD: {
			bit: 3,
			ON: 0x01,
			OFF: 0x00,
		},

		DBL_HEIGHT: {
			bit: 4,
			ON: 0x01,
			OFF: 0x00,
		},

		DBL_WIDTH: {
			bit: 5,
			ON: 0x01,
			OFF: 0x00,
		},

		UNDERLINE: {
			bit: 7,
			ON: 0x01,
			OFF: 0x00,
		},

	},

	// bold font
	CMD_SET_UNDERLINE			: [ 0x1b, 0x2d ],
	SET_UNDERLINE_OPTIONS			: {
		OFF		: 0x00,
		SINGLE	: 0x01,
		DOUBLE	: 0x02,
	},

	// bold font
	CMD_SET_BOLD				: [ 0x1b, 0x45 ],
	SET_BOLD_OPTIONS			: {
		OFF		: 0x00,
		ON		: 0x01,
	},

	// font type A or B
	CMD_SET_FONT				: [ 0x1b, 0x4d ],
	SET_FONT_OPTIONS			: {
		A		: 0x00,
		B		: 0x01,
	},

	// text alignment
	CMD_SET_ALIGNMENT				: [ 0x1b, 0x61 ],
	SET_ALIGNMENT_OPTIONS			: {
		LEFT	: 0x00,
		CENTER	: 0x01,
		RIGHT	: 0x02,
	},

	// Inverted font color
	TXT_INVERT_ON				: [ 0x1d, 0x42, 0x01 ],		// Inverted color text
	TXT_INVERT_OFF				: [ 0x1d, 0x42, 0x00 ],		// Inverted color text

	// font color
	TXT_COLOR_BLACK				: [ 0x1b, 0x72, 0x00 ],		// Default Color
	TXT_COLOR_RED				: [ 0x1b, 0x72, 0x01 ],		// Alternative Color (Usually Red)

	// Barcodes
	BARCODE_TXT_OFF			: [ 0x1d, 0x48, 0x00 ],		// HRI barcode chars OFF
	BARCODE_TXT_ABV			: [ 0x1d, 0x48, 0x01 ],		// HRI barcode chars above
	BARCODE_TXT_BLW			: [ 0x1d, 0x48, 0x02 ],		// HRI barcode chars below
	BARCODE_TXT_BTH			: [ 0x1d, 0x48, 0x03 ],		// HRI barcode chars both above and below
	BARCODE_FONT_A			: [ 0x1d, 0x66, 0x00 ],		// Font type A for HRI barcode chars
	BARCODE_FONT_B			: [ 0x1d, 0x66, 0x01 ],		// Font type B for HRI barcode chars
	BARCODE_HEIGHT			: [ 0x1d, 0x68 ],			// Barcode Height (1 - 255)
	BARCODE_WIDTH			: [ 0x1d, 0x77 ],			// Barcode Width (2 - 6)

	BARCODE_HEADER			: [ 0x1d, 0x6b ],
	BARCODE_TYPE			: {
		UPC_A				: [ 0x00 ],
		UPC_E				: [ 0x01 ],
		EAN13				: [ 0x02 ],
		EAN8				: [ 0x03 ],
		CODE39				: [ 0x04 ],
		ITF					: [ 0x05 ],
		NW7					: [ 0x06 ],
	},
};
